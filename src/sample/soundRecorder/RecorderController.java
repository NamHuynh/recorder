package sample.soundRecorder;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXSlider;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import sample.slider.Controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class RecorderController implements Initializable {

    @FXML
    private JFXButton btnPlay;

    @FXML
    private JFXButton btnControl;

    @FXML
    private Pane panelTimeLine;

    private Thread updaterThread;

    MediaPlayer mediaPlayer;

    SoundRecorder soundRecorder;

    double totalCurr;

    boolean isActive;

    Controller controller ;

    double totalSeconds;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnControl.setText("Recorder");
        btnControl.setBackground(new Background(new BackgroundFill(Color.rgb(133, 212, 255), CornerRadii.EMPTY, Insets.EMPTY)));

        btnPlay.setText("Play");
        btnPlay.setBackground(new Background(new BackgroundFill(Color.rgb(55, 228, 27), CornerRadii.EMPTY, Insets.EMPTY)));

//        timeRecorder.setText("00.00");


        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../slider/slider.fxml"));
            panelTimeLine.getChildren().add(loader.load());
//            panelTimeLine.getChildren().add(FXMLLoader.load(getClass().getResource("../slider/slider.fxml")));
            controller = loader.getController();

        } catch (IOException e) {
            e.printStackTrace();
        }
        controller.slider.setOnMouseClicked(event -> {
            setPos((controller.slider.getValue()/100) * totalCurr);
        });
    }

    public void triggerButton(){
        if (btnControl.getText().equals("Recorder")) {
            soundRecorder  = new SoundRecorder();
            Thread thread = new Thread(soundRecorder);
            thread.start();

            btnControl.setText("Stop");
            btnControl.setBackground(new Background(new BackgroundFill(Color.rgb(228, 67, 27), CornerRadii.EMPTY, Insets.EMPTY)));
        }
        else {
            btnControl.setText("Recorder");
            btnControl.setBackground(new Background(new BackgroundFill(Color.rgb(133, 212, 255), CornerRadii.EMPTY, Insets.EMPTY)));

            soundRecorder.stop();
        }
    }

    public void TriggerBtnPlay() {
        if (btnPlay.getText().equals("Play")) {
            btnPlay.setText("Pause");
            btnPlay.setBackground(new Background(new BackgroundFill(Color.rgb(80, 76, 255), CornerRadii.EMPTY, Insets.EMPTY)));
            playAudio();

        } else {
            btnPlay.setText("Play");
            btnPlay.setBackground(new Background(new BackgroundFill(Color.rgb(55, 228, 27), CornerRadii.EMPTY, Insets.EMPTY)));
            System.out.println("pause");
            pauseResume();
        }
    }

    Thread x;
    private void playAudio(){
        x = new Thread(new Task<Void>() {
            @Override
            protected Void call(){
                try {
                    Platform.runLater(()->{
                        controller.totalDurLabel.setText("00.00");
                        controller.nowDurLabel.setText("00.00");
                        controller.slider.setValue(0);
                        controller.slider.setDisable(true);
                    });
                    isActive = true;
                    String path = "RecordAudio.wav";
                    Media sound = new Media(new File(path).toURI().toString());
                    mediaPlayer = new MediaPlayer(sound);
                    mediaPlayer.setOnReady(()->{
                        totalCurr = sound.getDuration().toSeconds();

                        Platform.runLater(()->{
                            controller.slider.setDisable(false);
                            controller.totalDurLabel.setText(getSecondsToSimpleString(sound.getDuration().toSeconds()));
                        });
                        mediaPlayer.play();
                        System.out.println("play");
                        startUpdating();
                    });
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                return null;
            }
        });
        x.setPriority(1);
        x.start();
    }

    public void pauseResume()
    {
        new Thread(new Task<>() {
            @Override
            protected Object call() throws Exception {
                if(mediaPlayer.getStatus().equals(MediaPlayer.Status.PAUSED))
                {
                    mediaPlayer.play();
                }
                else if(mediaPlayer.getStatus().equals(MediaPlayer.Status.PLAYING))
                {
                    mediaPlayer.pause();
                }
                return null;
            }
        }).start();
    }

    public void setPos(double newDurSecs)
    {
        mediaPlayer.seek(new Duration(newDurSecs*1000));
    }

    private void startUpdating()
    {
        updaterThread = new Thread(new Task<Void>() {
            @Override
            protected Void call(){
                try
                {
                    while(isActive)
                    {
                        if(mediaPlayer.getStatus().equals(MediaPlayer.Status.PLAYING))
                        {
                            double currSec = mediaPlayer.getCurrentTime().toSeconds();
                            String currentSimpleTimeStamp = getSecondsToSimpleString(currSec);
                            Platform.runLater(()->controller.nowDurLabel.setText(currentSimpleTimeStamp));

                            double currentProgress = (currSec/totalCurr)*100;

                            if(!controller.slider.isValueChanging())
                            {
                                controller.slider.setValue(currentProgress);
                            }

                            if(currSec == totalCurr){
                                isActive = false;
                                System.out.println("stop");
                            };
                        }
                        Thread.sleep(500);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                return null;
            }
        });
        updaterThread.start();
    }

    public String getSecondsToSimpleString(double Seconds)
    {
        double mins = Seconds/60;
        String minsStr = mins + "";
        int index = minsStr.indexOf('.');
        String str1 = minsStr.substring(0,index);
        String minsStr2 = minsStr.substring(index+1);
        double secs = Double.parseDouble("0." + minsStr2) * 60;
        String str2 = (int) secs+"";
        if(secs<10) str2 = 0 + str2;
        return str1+":"+str2;
    }

}
