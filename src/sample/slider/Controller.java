package sample.slider;

import com.jfoenix.controls.JFXSlider;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    public Label nowDurLabel;

    @FXML
    public JFXSlider slider;

    @FXML
    public Label totalDurLabel;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        nowDurLabel.setText("00.00");
        totalDurLabel.setText("00.00");
        slider.setValue(0);
        slider.setMin(0);
        slider.setMax(100);
    }

    public String getSecondsToSimpleString(double Seconds)
    {
        double mins = Seconds/60;
        String minsStr = mins + "";
        int index = minsStr.indexOf('.');
        String str1 = minsStr.substring(0,index);
        String minsStr2 = minsStr.substring(index+1);
        double secs = Double.parseDouble("0." + minsStr2) * 60;
        String str2 = (int) secs+"";
        if(secs<10) str2 = 0 + str2;
        return str1+":"+str2;
    }
}
